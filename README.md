# Diabète Prédiction



## Présentation

Projet de Deep Learning de prédiction du diabète.



## Description

* Projet python utilisant Keras et TensorFlow développant un réseau de neurone permettant la prédiction du diabète à partir de données traitées avec Pandas. Modele de prédiction de 70% de moyenne basé sur les informations médicales de 789 patients.
* Prediction du diabete avec un modele de classification de sklearn (KNeighborsClassifier).

## Technologie

* Python 
* Pandas
* Keras
* TensorFlow
* Matplolib
* Dataflore (Jupyter)
* sklearn
* Numpy

## Data

Utilisation d'un csv avec les données medicales de 789 patients.


